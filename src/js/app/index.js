import React, { Component } from 'react';
import IconImage from 'images/icon.png';

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='hero'>
        <h1>FRUIT MACHINE</h1>
        <div className="slot">one</div>
        <div className="slot">two</div>
        <div className="slot">three</div>
      </div>
    )
  }
}

export default App
